"""This module contains the KeyDown proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class KeyDown(ActionProxy):
    def __init__(self, theKey: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.actions.KeyDown"
        )
        self.theKey = theKey
