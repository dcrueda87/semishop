"""This module contains the SetItemInLocalStorage proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class SetItemInLocalStorage(ActionProxy):
    def __init__(self, key: str, value: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.localstorage.SetItemInLocalStorage"
        )
        self.key = key
        self.value = value
