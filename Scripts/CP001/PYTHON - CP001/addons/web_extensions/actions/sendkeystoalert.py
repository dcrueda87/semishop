"""This module contains the SendKeysToAlert proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class SendKeysToAlert(ActionProxy):
    def __init__(self, keys: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.alert.SendKeysToAlert"
        )
        self.keys = keys
