"""This module contains the WaitForAlertPresence proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class WaitForAlertPresence(ActionProxy):
    def __init__(self, timeout: int):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.alert.WaitForAlertPresence"
        )
        self.timeout = timeout
