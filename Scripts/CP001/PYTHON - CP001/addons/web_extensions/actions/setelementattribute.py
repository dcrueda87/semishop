"""This module contains the SetElementAttribute proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class SetElementAttribute(ActionProxy):
    def __init__(self, attributeName: str, attributeValue: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.element.SetElementAttribute"
        )
        self.attributeName = attributeName
        self.attributeValue = attributeValue
