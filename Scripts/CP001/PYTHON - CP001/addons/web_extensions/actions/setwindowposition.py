"""This module contains the SetWindowPosition proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class SetWindowPosition(ActionProxy):
    def __init__(self, x: str, y: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.window.SetWindowPosition"
        )
        self.x = x
        self.y = y
