"""This module contains the SelectOptionbyVisibleText proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class SelectOptionbyVisibleText(ActionProxy):
    def __init__(self, value: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.element.select.SelectOptionbyVisibleText"
        )
        self.value = value
