"""This module contains the ExecuteJavascript proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class ExecuteJavascript(ActionProxy):
    def __init__(self, code: str, args: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.actions.ExecuteJavascript"
        )
        self.code = code
        self.args = args
        self.output = None
