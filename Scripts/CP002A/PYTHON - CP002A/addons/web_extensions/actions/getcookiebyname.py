"""This module contains the GetCookieByName proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class GetCookieByName(ActionProxy):
    def __init__(self, name: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.cookies.GetCookieByName"
        )
        self.name = name
        self.value = None
        self.expiry = None
        self.path = None
        self.domain = None
        self.isSecure = None
