"""This module contains the DeselectOptionByIndex proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class DeselectOptionByIndex(ActionProxy):
    def __init__(self, index: int):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.element.select.DeselectOptionByIndex"
        )
        self.index = index
