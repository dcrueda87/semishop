"""This module contains the SetWindowSize proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class SetWindowSize(ActionProxy):
    def __init__(self, width: str, height: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.window.SetWindowSize"
        )
        self.width = width
        self.height = height
