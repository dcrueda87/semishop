"""This module contains the GetCssValue proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class GetCssValue(ActionProxy):
    def __init__(self, propertyName: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.element.GetCssValue"
        )
        self.propertyName = propertyName
        self.value = None
