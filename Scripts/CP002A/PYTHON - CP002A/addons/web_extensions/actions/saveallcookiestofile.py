"""This module contains the SaveAllCookiesToFile proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class SaveAllCookiesToFile(ActionProxy):
    def __init__(self, folderPath: str, fileName: str = "Cookies"):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.cookies.SaveAllCookiesToFile"
        )
        self.folderPath = folderPath
        self.fileName = fileName
