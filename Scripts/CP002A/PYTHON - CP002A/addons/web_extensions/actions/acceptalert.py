"""This module contains the AcceptAlert proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class AcceptAlert(ActionProxy):
    def __init__(self, pause: int):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.alert.AcceptAlert"
        )
        self.pause = pause
