"""This module contains the RemoveItemFromLocalStorage proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class RemoveItemFromLocalStorage(ActionProxy):
    def __init__(self, key: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.localstorage.RemoveItemFromLocalStorage"
        )
        self.key = key
