"""This module contains the DeselectOptionsByValue proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class DeselectOptionsByValue(ActionProxy):
    def __init__(self, value: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.element.select.DeselectOptionsByValue"
        )
        self.value = value
