from selenium.webdriver.common.by import By
from src.testproject.classes import DriverStepSettings, StepSettings
from src.testproject.decorator import report_assertion_errors
from src.testproject.enums import SleepTimingType
from src.testproject.sdk.drivers import webdriver
import pytest


"""
This pytest test was automatically generated by TestProject
    Project: My first Project
    Package: TestProject.Generated.Tests.MyFirstProject
    Test: CP005
    Generated by: sebastian martinez (sebastianmartinez456@gmail.com)
    Generated on 10/12/2021, 03:03:07
"""


@pytest.fixture()
def driver():
    driver = webdriver.Chrome(token="K-dyHDV5rVTj44vdotAGO1Fkad2wxpr4QBFGJoShNHc",
                              project_name="My first Project",
                              job_name="CP005")
    step_settings = StepSettings(timeout=15000,
                                 sleep_time=500,
                                 sleep_timing_type=SleepTimingType.Before)
    with DriverStepSettings(driver, step_settings):
        yield driver
    driver.quit()


@report_assertion_errors
def test_main(driver):
    """Seleccionar método de pago."""
    # Test Parameters
    # Auto generated application URL parameter
    ApplicationURL = "http://localhost/freshshop/index.php"
    correo = "cquintero98@gmail.com"
    clave = "123456"
    cantidad = "5"
    pagar = "455"

    # 1. Navigate to '{ApplicationURL}'
    # Navigates the specified URL (Auto-generated)
    driver.get(f'{ApplicationURL}')

    # 2. Scroll window by ('0','1200')
    driver.execute_script("window.scrollBy(0,1200)")

    # 3. Click 'AÑADIR AL CARRITO'
    a_adir_al_carrito = driver.find_element(By.XPATH,
                                            "//div[1]/a[. = 'AÑADIR AL CARRITO']")
    a_adir_al_carrito.click()

    # 4. Click 'correo'
    correo = driver.find_element(By.CSS_SELECTOR,
                                 "#correo")
    correo.click()

    # 5. Type '{correo}' in 'correo'
    correo = driver.find_element(By.CSS_SELECTOR,
                                 "#correo")
    correo.send_keys(f'{correo}')

    # 6. Click 'clave'
    clave = driver.find_element(By.CSS_SELECTOR,
                                "#clave")
    clave.click()

    # 7. Type '{clave}' in 'clave'
    clave = driver.find_element(By.CSS_SELECTOR,
                                "#clave")
    clave.send_keys(f'{clave}')

    # 8. Click 'Validar'
    validar = driver.find_element(By.XPATH,
                                  "//button[. = 'Validar']")
    validar.click()

    # 9. Scroll window by ('0','100')
    driver.execute_script("window.scrollBy(0,100)")

    # 10. Scroll window by ('0','200')
    driver.execute_script("window.scrollBy(0,200)")

    # 11. Scroll window by ('0','200')
    driver.execute_script("window.scrollBy(0,200)")

    # 12. Scroll window by ('0','100')
    driver.execute_script("window.scrollBy(0,100)")

    # 13. Scroll window by ('0','100')
    driver.execute_script("window.scrollBy(0,100)")

    # 14. Scroll window by ('0','200')
    driver.execute_script("window.scrollBy(0,200)")

    # 15. Scroll window by ('0','100')
    driver.execute_script("window.scrollBy(0,100)")

    # 16. Scroll window by ('0','100')
    driver.execute_script("window.scrollBy(0,100)")

    # 17. Scroll window by ('0','300')
    driver.execute_script("window.scrollBy(0,300)")

    # 18. Scroll window by ('0','100')
    driver.execute_script("window.scrollBy(0,100)")

    # 19. Click 'AÑADIR AL CARRITO1'
    a_adir_al_carrito1 = driver.find_element(By.XPATH,
                                             "//div[2]/a[. = 'AÑADIR AL CARRITO']")
    a_adir_al_carrito1.click()

    # 20. Click 'INPUT'
    input = driver.find_element(By.XPATH,
                                "//input")
    input.click()

    # 21. Type '{cantidad}' in 'INPUT'
    input = driver.find_element(By.XPATH,
                                "//input")
    input.send_keys(f'{cantidad}')

    # 22. Click 'Realizar pago'
    realizar_pago = driver.find_element(By.XPATH,
                                        "//button[. = 'Realizar pago']")
    realizar_pago.click()

    # 23. Click 'concept'
    concept = driver.find_element(By.CSS_SELECTOR,
                                  "#concept")
    concept.click()

    # 24. Type 'mercado' in 'concept'
    concept = driver.find_element(By.CSS_SELECTOR,
                                  "#concept")
    concept.send_keys("mercado")

    # 25. Click 'amount'
    amount = driver.find_element(By.CSS_SELECTOR,
                                 "#amount")
    amount.click()

    # 26. Type '{pagar}' in 'amount'
    amount = driver.find_element(By.CSS_SELECTOR,
                                 "#amount")
    amount.send_keys(f'{pagar}')

    # 27. Click 'submitPayment'
    submitpayment = driver.find_element(By.CSS_SELECTOR,
                                        "[name='submitPayment']")
    submitpayment.click()

    # 28. Click 'login_email'
    login_email = driver.find_element(By.CSS_SELECTOR,
                                      "#email")
    login_email.click()

    # 29. Type '{correo}' in 'login_email'
    login_email = driver.find_element(By.CSS_SELECTOR,
                                      "#email")
    login_email.send_keys(f'{correo}')

    # 30. Click 'login'
    login = driver.find_element(By.CSS_SELECTOR,
                                "#login")
    login.click()
